<?php

namespace Dbms {

use \Closure;
use \AutoLoader;
use \Template;
use \Exception;
use \InvalidArgumentException;

class Migration {

    public $schemas;
    public $schema_dir;
    public $adapter;
    public $dry_run = false;
    private $current_table;
    static $this_dir;

    function __construct()
    {
        Migration::$this_dir = dirname(__file__);
        $this->schemas = array();
    }

    function use_connection($url)
    {
        $u = Adapters\Connection::parse_url($url);

        if (empty($u['scheme']))
            throw new Exception('invalid connection url.');

        $adapter_class = 'Dbms\\Adapters\\' .
            AutoLoader::to_camel_case($u['scheme']);

        $this->adapter = new $adapter_class();

        $this->adapter->connect($url);

    }

    function load_schema($name)
    {
        $class_name = AutoLoader::to_underscore_case($name);

        if (empty($name)) throw new InvalidArgumentException(
            'No schema name provided.'
        );

        if (isset($this->schemas[$name]))
        {
            return true;
        } 
        else
        {
            if (empty($this->schema_dir)) 
            {
                throw new Exception(
                    'Schema directory is not defined. '.
                    "failed to load: $name"
                    );
            }

            $file = $this->schema_dir . "/" . 
                $class_name . ".schema.php";

            if (!file_exists($file)) throw new Exception(
                "Schema record file not found for: $name"
            );

            require_once($file);

            $this->schemas[$name] = new $name();
        }
    }

    public function run($sql)
    {
        echo "===========================\n";
        echo $sql;
        echo "===========================\n";

        if (!$this->dry_run)
        {
            if (!$this->adapter)
            {
                throw new Exception(
                    "No database connection established. "
                    . "Perhaps you would like to set dry_run"
                    );
            }
            return $this->adapter->execute_sql($sql);
        }
    }

    public function create_table($table_name)
    {
        $this->load_schema($table_name);
       
        $args = array(
            'schema' => $this->schemas[$table_name]
        );

        $query = $this->query_string(
            'schema_create_table.template.sql',
            $args
            );

        $this->run($query);
    }

    public function drop_table($table_name)
    {
        $this->load_schema($table_name);
       
        $args = array(
            'schema' => array(
                'object' => $this->schemas[$table_name]
                )
            );

        $query = $this->query_string(
            'schema_drop_table.template.sql',
            $args
            );

        $this->run($query);
    }

    public function add_column($table, $column_name, $options = null)
    {
        $this->load_schema($table);

        $this->schemas[$table]->add_attribute(
            $column_name,
            $options
        );

        $args = array (
            'schema' => array (
                'name' => $this->schemas[$table]->safe_name($table),
                'column' =>
                    $this->schemas[$table]->attributes[$column_name]
                )
            );

        $this->run(
            $this->query_string(
                'schema_alter_table_add_column.template.sql',
                $args
                )
            );
    }

    public function rename_column(
        $table, $old_column_name, $new_column_name
        )
    {
        $this->load_schema($table);

        $old_column = $this->schemas[$table]->rename_attribute(
            $old_column_name,
            $new_column_name
        );

        $args = array(
            'schema' => $this->schemas[$table],
            'old_column' => $old_column,
            'new_column' =>
                $this->schemas[$table]->attributes[$new_column_name]
            );

        $this->run(
            $this->query_string(
                'schema_alter_table_rename_column.template.sql',
                $args
                )
            );
    }

    public function change_column($table, $column_name, $options = null)
    {
        $this->load_schema($table);

        $this->schemas[$table]->add_attribute(
            $column_name,
            $options,
            true
        );

        $args = array(
            'schema' => array(
                    'object' => $this->schemas[$table],
                    'column' =>
                        $this->schemas[$table]->attributes[$column_name]
                )
            );

        $this->run(
            $this->query_string(
                'schema_alter_table_alter_column.template.sql',
                $args
                )
            );
    }

    public function remove_column($table, $column_name)
    {
        $this->load_schema($table);

        $this->schemas[$table]->verify_has_attribute($column_name);

        $args = array(
            'schema' => array(
                'object' => $this->schemas[$table],
                'column' => 
                    $this->schemas[$table]->attributes[$column_name]
                )
            );

        $this->run(
            $this->query_string(
                'schema_alter_table_remove_column.template.sql',
                $args
                )
            );

        $this->schemas[$table]->remove_attribute($column_name);
    }

    public function add_unique($table, $columns, $options = null)
    {
       $this->load_schema($table); 

       $key = $this->schemas[$table]->add_unique($columns, $options);

       $args = array(
           'schema' => array(
                'object' => $this->schemas[$table],
                'key' => $key
                )
           );

       $this->run(
           $this->query_string(
               'schema_alter_table_add_key_unique.template.sql',
               $args
               )
           );
    }

    public function add_primary($table, $columns, $options = null)
    {
       $this->load_schema($table); 

       $key = $this->schemas[$table]->add_primary($columns, $options);

       $args = array(
           'schema' => array(
                'object' => $this->schemas[$table],
                'key' => $key
                )
           );

       $this->run(
           $this->query_string(
               'schema_alter_table_add_key_primary.template.sql',
               $args
               )
           );
    }

    public function add_foreign(
        $table,
        $referenced_table, 
        $column_names, 
        $referenced_column_names = null,
        $options = null
        )
    {
        (!is_null($referenced_column_names)) or 
            ($referenced_column_names = $column_names);

        $this->load_schema($table);
        $this->load_schema($referenced_table);

        $key = $this->schemas[$table]->add_foreign(
            $referenced_table,
            $column_names,
            $referenced_column_names,
            $options
        );

        $args = array(
            'schema' => array(
                'object' => $this->schemas[$table],
                'key' => $key
                )
            );

       
        $this->run(
            $this->query_string(
               'schema_alter_table_add_key_foreign.template.sql',
               $args
               )
           );
    }

    public function remove_key($table, $alias)
    {
        $this->load_schema($table);

        $key = $this->schemas[$table]->remove_key(
            $alias
        );

        $args = array(
            'schema' => array(
                'object' => $this->schemas[$table],
                'key' => $key
                )
            );

        $template_file = "schema_alter_table_remove_key_" .
            $key->type . ".template.sql";

        $this->run(
            $this->query_string(
                $template_file,
                $args
                )
            );
    }

    public function query_string($file, $args)
    {
        $template = new Template($args);

        $template->load_template_file(
                    Migration::$this_dir . '/templates/mysql/' .
                    $file 
                );

        return $template->render_as_string( array(
                'post_process' => array(
                      Template::compact_multilines()
                    )
                )
            );
    }

    # save table functions

    function save_table($table_name)
    {
        $args = array(
            'schema' => $this->schemas[$table_name]
            );

        $file_name = $this->schema_dir . '/' .
            $this->schemas[$table_name]->safe_name() . ".schema.php";

        file_put_contents(
            $file_name,
            $this->query_string(
                "schema_save.template.sql",
                $args
                )
            );
    }

} # end Migration

} # end Dbms
