<?php

class MigrationTest extends PHPUnit_Extensions_OutputTestCase 
{
    function setUp()
    {
        $this->migration = new Dbms\Migration();
        $this->migration->use_connection(
            'mysql://localhost/test'
            );
        $this->migration->dry_run = true;
        $this->person = new Dbms\Schema('Person');
        $this->migration->schemas['Person'] = $this->person;
    }

    function test_create_table()
    {
        $this->person->add_foreign(
            'Table4',
            'id'
            );

        $expected = file_get_contents(
            FIXTURES_DIR . "/create_table/expected.sql"
            );

        $this->expectOutputString($expected);

        $this->migration->create_table('Person');
    }

    function test_drop_table()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/drop_table/expected.sql"
            );

        $this->expectOutputString($expected);

        $this->migration->drop_table('Person');
    }

    function test_add_column()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/add_column/expected.sql"
            );

        $this->expectOutputString($expected);

        $this->migration->add_column(
            'Person',
            'name',
            array(
                'type' => 'string',
                'length' => 10,
                'not_null' => true,
                'default' => "'Mr. Cow'"
                )
            );
    }

    function test_rename_column()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/rename_column/expected.sql"
            );
        $this->expectOutputString($expected);

        $column = new Dbms\Column();
        $column->name = 'name';
        $column->type = 'string';
        $column->length = 10;
        $column->not_null = true;
        $column->default = "'Mr. Cow'";

        $this->migration->schemas['Person']->attributes['name'] = $column;

        $this->migration->rename_column(
            'Person',
            'name',
            'first_name'
            );
    }

    function test_change_column()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/change_column/expected.sql"
            );

        $this->expectOutputString($expected);

        $column = new Dbms\Column();
        $column->name = 'name';
        $column->type = 'string';
        $column->length = 10;
        $column->not_null = true;
        $column->default = "'Mr. Cow'";
        $this->migration->schemas['Person']->attributes['name'] = $column;

        $this->migration->change_column(
            'Person',
            'name',
            array(
                'type' => 'string',
                'length' => 20,
                'not_null' => true,
                'default' => "'Mr. Cow'"
                )
            );
    }

    function test_remove_column()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/remove_column/expected.sql"
            );

        $this->expectOutputString($expected);

        $column = new Dbms\Column();
        $column->name = 'name';
        $column->type = 'string';
        $column->length = 10;
        $column->not_null = true;
        $column->default = "'Mr. Cow'";
        $this->migration->schemas['Person']->attributes['name'] = $column;

        $this->migration->remove_column(
            'Person',
            'name'
            );
    }

    function test_add_unique()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/add_unique/expected.sql"
            );

        $this->expectOutputString($expected);

        $this->migration->add_unique('Person', 'name');
    }

    function test_add_primary()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/add_primary/expected.sql"
            );

        $this->expectOutputString($expected);

        $this->migration->add_primary('Person', 'name');
    }

    function test_add_foreign()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/add_foreign/expected.sql"
            );

        $this->expectOutputString($expected);

        $table4 = new Dbms\Schema('Table4');
        $this->migration->schemas['Table4'] = $table4;

        $this->migration->add_foreign('Person', 'Table4','id');
    }

    function test_remove_unique_key()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/remove_key/unique_expected.sql"
            );

        $this->expectOutputString($expected);

        $this->migration->remove_key(
            'Person',
            'uk_person_transaction_sha'
            );
    }

    function test_remove_primary_key()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/remove_key/primary_expected.sql"
            );

        $this->expectOutputString($expected);

        $this->migration->remove_key(
            'Person',
            'pk_person_id'
            );
    }

    function test_remove_foreign_key()
    {
        $expected = file_get_contents(
            FIXTURES_DIR . "/remove_key/foreign_expected.sql"
            );

        $this->migration->schemas["Table4"] = new Dbms\Schema("Table4");
        $this->migration->schemas["Person"]->add_foreign(
            'Table4',
            'id'
            );

        $this->expectOutputString($expected);

        $this->migration->remove_key(
            'Person',
            'fk_person_id_on_table4_id'
            );
    }
}
