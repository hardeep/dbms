<?php

namespace Dbms\Adapters {

abstract class Base extends Connection 
{
    public $column_types = array(
        'string' => 'VARCHAR',
        'text' => 'TEXT',
        'integer' => 'INT',
        'float' => 'FLOAT',
        'decimal' => 'DECIMAL',
        'timestamp' => 'TIMESTAMP',
        'time' => 'DATETIME',
        'date' => 'DATE',
        'binary' => "BLOB",
        'boolean' => 'BOOL'
    );

    public $key_types  = array(
        'primary' => array(
            'alias' => 'pk'
            ),
        'unique' => array(
            'alias' => 'uc'
            ),
        'foreign' => array(
            'alias' => 'fk'
            )
    );

    public $attr_properties = array(
        'name',
        'type',
        'length',
        'default',
        'char_set',
        'collation',
        'not_null',
        'engine',
        'auto_inc',
        );
} # end Base

} # end Dbms\Adapter
