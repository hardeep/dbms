<?php

class {{schema_name}}Migration extends Migration{

  function up()
  {
      $schema = new {{ schema_name }}();

      {% foreach value in keys_to_change.remove %}
      $this->remove_key($schema, "{{ value.name }}", "{{ value.type }}");
      {% endfor %}

      {% foreach value in attributes_to_change.remove %}
      $this->remove_column($schema, "{{ value }}");
      {% endfor %}

      {% foreach value in attributes_to_change.add %}
      $this->add_column($schema, "{{ value }}");
      {% endfor %}

      {% foreach value in attributes_to_change.rename %}
      $this->rename_column(
              $schema,
              "{{ value.old_name }}",
              "{{ value.new_name }}"
          );
      {% endfor %}

      {% foreach value in attributes_to_change.change %}
      $this->alter_column($schema, "{{ value }}");
      {% endfor %}

      {% foreach value in keys_to_change.add %}
      $this->add_key($schema, "{{ value.name }}", "{{ value.type }}");
      {% endfor %}
  }
}
