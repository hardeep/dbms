<?php 

require_once 'lib/auto_loader.class.php';
require_once 'lib/template.class.php';
require_once 'lib/exceptions/file_exception.class.php';
require_once 'lib/array_obj.class.php';
require_once 'lib/base_class.class.php';
require_once 'lib/adapters/connection.class.php';
require_once 'lib/adapters/base.class.php';
require_once 'lib/adapters/mysql.class.php';
require_once 'lib/column.class.php';
require_once 'lib/schema.class.php';
require_once 'lib/migration.class.php';
require_once 'lib/key.class.php';
require_once 'lib/foreign_key.class.php';
require_once 'lib/primary_key.class.php';
require_once 'lib/unique_key.class.php';

define('FIXTURES_DIR', dirname(__FILE__) . "/tests/fixtures");
