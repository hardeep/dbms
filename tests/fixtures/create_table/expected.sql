===========================
CREATE TABLE person
(
    `id` INT 
        NOT NULL
        AUTO_INCREMENT
        ,
    `transaction_sha` VARCHAR (40)
    , CONSTRAINT pk_person_id PRIMARY KEY (`id`)  
    , CONSTRAINT uk_person_transaction_sha UNIQUE KEY (`transaction_sha`)  
    , CONSTRAINT fk_person_id_on_table4_id FOREIGN KEY (`id`)
      REFERENCES table4(`id`)
)
CHARACTER SET utf8 COLLATE utf8_unicode_ci;
SET storage_engine=innoDB;
===========================
