<?php

namespace Dbms\Adapters {

use \InvalidArgumentException;
use \PDO;

class Mysql extends Base 
{
    static $qoute = '`';
    static $default_port = 3306;
    public $engine = 'innoDB';

    static function connect($url)
    {
        $url = Mysql::parse_url($url); 
        $url = static::interpret_url($url);

        try
        {
            static::$instance = new PDO(
                $url['url'],
                $url['user'],
                $url['pass'],
                static::$PDO_OPTIONS
                );
        }
        catch (Exception $e)
        {
            throw new Exception($e->getMessage());
        }
    }

    static function interpret_url($u)
    {
        if (!isset($u['path']))
        {
            throw new InvalidArgumentException(
                'No database specified in the connection url provided'
                );
        }

        $u['path'] = substr($u['path'], 1);
        $u['host'] = ($u['host'] == "localhost")?"127.0.0.1":$u['host'];
        $u['user'] = (empty($u['user']))?null:$u['user'];
        $u['pass'] = (empty($u['pass']))?null:$u['pass'];

        (!empty($u['port'])) or 
            ($u['port'] = static::$default_port);

        $url = 'mysql:host=' . $u['host'] . 
            ';port=' . $u['port'] .
            ';dbname=' . $u['path']; 

        return array(
            'url' => $url,
            'user' => $u['user'],
            'pass' => $u['pass']
            );
    }
} # end Mysql

} # end Dbms\Adapters
