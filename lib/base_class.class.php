<?php

namespace Nodv {

use \ReflectionClass;
use \InvalidArgumentException;

abstract class BaseClass 
{
    # public read/write visibility for attributes
    private $public_attributes = array();

    # filter structure for methods
    private $before_filter = array();
    private $after_filter = array();

    # filter structure for properties
    private $after_get_filter = array();
    private $before_set_filter = array();

    protected $data = array();

    # constrcutor

    function __construct() {} 

    # visibility resolution methods

    function attr_accessable($attributes = array())
    {
        $attributes = func_get_args();

        (is_array($attributes)) or
            ($attributes = array($attributes));

        foreach ($attributes as $attribute)
        {
            if (!array_key_exists(
                $attribute, 
                $this->public_attributes
                ))
            {
                # will treat this as hash as it's a faster lookup
                # then using in_array
                $this->public_attributes[$attribute] = null;
            }
        }
    }

    function is_accessable_property($attr)
    {
        if (!array_key_exists($attr,$this->public_attributes))
        {
            throw new InvalidArgumentException(
                "Tried to access private or non existent element ".
                $attr
                );
        }
    }

    # filter methods

    function destroy_filters($type)
    {
        $this->{$type} = array();
    }

    function show_filters($type)
    {
        return $this->{$type};
    }

    # getter methods

    function __get($name)
    {
        $this->is_accessable_property($name);

        if (array_key_exists($name, $this->data))
        {
            $arg = $this->data[$name];

            if (array_key_exists($name, $this->after_get_filter))
            {
                return $this->run_filters(
                        'after_get_filter',
                        $name,
                        $arg
                        );
            }

            return $arg;
        }
        
        return null;
    }

    function after_get_filter($filter, $properties)
    {
        $this->add_filter(
            static::remove_namespace(__method__),
            $filter,
            $properties
            );
    }

    # setter methods

    function __set($name, $value)
    {
        $this->is_accessable_property($name);

        if (array_key_exists($name, $this->before_set_filter))
        {
            $value = $this->run_filters(
                    'before_set_filter',
                    $name,
                    $value
                    );
        }

        $this->data[$name] = $value;
    }

    function before_set_filter($filter, $properties)
    {
        $this->add_filter(
            static::remove_namespace(__method__),
            $filter,
            $properties
            );
    }

    # method functions

    static function remove_namespace($method_or_class)
    {
        $class = join("\\", explode("::", $method_or_class));
        return end(explode("\\", $class));
    }

    function before_filter($filter, $on_methods)
    {
        $this->add_filter(
            static::remove_namespace(__method__),
            $filter,
            $on_methods
            );
    }

    function after_filter($filter, $on_methods)
    {
        $this->add_filter(
            static::remove_namespace(__method__),
            $filter,
            $on_methods
            );
    }

    function add_filter($type, $filters, $on_methods)
    {
        (is_array($on_methods)) or
            ($on_methods = array($on_methods));

        (is_array($filters)) or
            ($filters = array($filters));
            
        (property_exists($this, $type)) or
            ($this->$type = array());

        foreach($on_methods as $method)
        {
            foreach($filters as $filter)
            {
                (isset($this->{$type}[$method])) or
                    ($this->{$type}[$method] = array());
                
                array_push($this->{$type}[$method], $filter);
            }
        }
    }

    function run_filters($type, $method, $arg = null)
    {
        if (isset($this->{$type}[$method]))
        {
            return $this->_run_filters(
                    $this->{$type}[$method],
                    $arg
                    );
        }
    }

    private function _run_filters($methods_chain, $arg)
    {
        $method = array_shift($methods_chain);

        if (count($methods_chain) > 0)
        {
            $arg = $this->{$method}($arg);
            return $this->_run_filters($methods_chain, $arg);
        }
        else
        {
            return $this->{$method}($arg);
        }
    }

    function __call($method, $args = array())
    {
        if (method_exists($this, $method))
        {
            $this->run_filters('before_filter', $method);

            ### start saftey check #######################
            $class = get_called_class();
            $reflector = new ReflectionClass($class);

            if ($reflector->getMethod($method)->isPrivate())
            {
                throw new InvalidArgumentException(
                    "method $method " .
                    "has been defined as private. This could ".
                    "cause a segfault."
                    );
            }
            #### end saftey check #########################

            $value = call_user_func_array(
                array($this, $method),
                $args
                );

            $this->run_filters('after_filter' , $method);

            return $value;
        }

        throw new InvalidArgumentException(
            "Method $method does not exist" 
            );
    }
} # end BaseClass 

} # end Nodv 
