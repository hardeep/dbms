ALTER TABLE {{schema.safe_name}} 
    ADD CONSTRAINT {{schema.key.alias}} 
    FOREIGN KEY ({{schema.key.columns}})
    REFERENCES {{schema.key.referenced.schema}} ({{schema.key.referenced.columns}})
;
