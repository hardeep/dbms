<?php

namespace Dbms {

use \PHPUnit_Framework_TestCase;

class SchemaTest extends PHPUnit_Framework_TestCase
{
    function test_object_exists()
    {
        $column = new Schema(); 
        $this->assertEquals(get_class($column), 'Dbms\\Schema');
    }

    function setUp()
    {
        $this->schema = new Schema('Person'); 
    }

    function test_add_attribute()
    {
        $this->schema->add_attribute(
            'name',
            array('length' => 10, 'not_null' => true)
            );
        $this->assertEquals(
            $this->schema->attributes['name']->length, 10
            );
        $this->assertEquals(
            $this->schema->attributes['name']->not_null, true 
            );
    }

    function test_add_primary_key()
    {
        $this->schema->add_primary('id');
        $this->assertEquals(
            isset($this->schema->keys['primary']['id']),
            true
            );

        $this->assertEquals(
            $this->schema->keys['primary']['id']->alias,
            'pk_person_id'
            );
    }

    function test_unique_key()
    {
        $this->schema->add_unique('id');

        $this->assertEquals(
            isset($this->schema->keys['unique']['id']),
            true
            );

        $this->assertEquals(
            $this->schema->keys['unique']['id']->alias,
            'uk_person_id'
            );
    }

    function test_add_foreign_key()
    {
        $schema = new Schema('Home');

        $this->schema->add_foreign(
            'Home', 'id'
            );

        $this->assertEquals(
            isset($this->schema->keys['foreign']['id']),
            true
            );

        $this->assertEquals(
            $this->schema->keys['foreign']['id']->alias,
            'pk_person_id_on_home_id'
            );
    }
} # end SchemaTest

} # end Dbms
