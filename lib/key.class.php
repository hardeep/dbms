<?php

namespace Dbms\Keys {

use Nodv\BaseClass;
use \AutoLoader;

abstract class Key extends BaseClass
{
    function __construct()
    {
        $this->attr_accessable(
            'table',
            'name',
            'type',
            'alias',
            'adapter',
            'columns',
            'adapter_class'
            );

        $this->type = null;

        $this->before_set_filter('validate_columns', 'columns');
        $this->before_filter('validate_adapter', 'columns_to_s');
        $this->before_set_filter('set_adapter_class', 'adapter');
        $this->before_set_filter(
            'safe_name',
            array('name', 'columns', 'table')
            );
        $this->after_get_filter('alias_to_s', 'alias');
        $this->after_get_filter('type_to_s', 'type');
    }

    protected function validate_adapter()
    {
        return (empty($this->adapter) && 
            (preg_match("/Adapter$/", get_class($this->adapter))));
    }

    protected function columns_to_s()
    {
        $class = $this->adapter_class;
        $q = $class::$qoute;
        return $q . implode("$q,$q", $this->columns) . $q;
    }

    protected function set_adapter_class($arg)
    {
        $this->adapter_class = get_class($arg);
        return $arg;
    }

    protected function safe_name($args)
    {
        if (empty($args)) return null;

        $safe_pattern = "/[^a-zA-Z0-9_]/";
        $replace_with = '_';

        if (is_array($args))
        { 
            foreach($args as &$arg)
            {
                $arg = strtolower(
                    preg_replace($safe_pattern, $replace_with, $arg)
                    );
            }
        }
        else
        {
            $args = strtolower(
                preg_replace($safe_pattern, $replace_with, $args)
                );
        }

        return $args;
    }

    abstract public function alias_to_s($arg);

    protected function validate_columns($columns)
    {
        if (empty($columns)) return null;
        if (is_array($columns)) return $columns;
        return array($columns);
    }

    function type_to_s()
    {
        # yeah this is some damn awesome magic
        $class = explode(
            '_', AutoLoader::to_underscore_case(
                static::remove_namespace(get_class($this))
            ));

        array_pop($class);

        return join('_', $class);
    }
} # end Key

} # end Dbms
