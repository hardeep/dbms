===========================
ALTER TABLE person    
    CHANGE `name` `first_name` VARCHAR (10)
    NOT NULL
    DEFAULT 'Mr. Cow'
;
===========================
