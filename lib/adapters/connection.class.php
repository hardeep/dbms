<?php

namespace Dbms\Adapters {

use \PDO;
use \Exception;

abstract class Connection
{
    static $instance;
    static $use_presistant = false;

    static $PDO_OPTIONS = array(
        PDO::ATTR_CASE => PDO::CASE_NATURAL,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL
        );

    public function __construct()
    {
        $this->instance = null;
        $this->use_presistent = false;
    }

    static function parse_url($connection_url)
    {
        return parse_url($connection_url);  
    }

    abstract static function interpret_url($peices);

    abstract static function connect($info);

    public function execute_sql($sql)
    {
       return static::$instance->exec($sql); 
    }
} # end Connection

} # end Dbms
