ALTER TABLE {{ schema.name }}    
    ADD {{schema.column.name_to_s }} {{schema.column.type_to_s }} {{schema.column.length_to_s }}

    {{ schema.column.not_null_to_s }}

{% if schema.column.default %}
    DEFAULT {{schema.column.default_to_s}}
{% endif %}

    {{ schema.column.auto_inc_to_s }}

{% if schema.column.char_set %} 
    CHARACTER SET {{schema.column.char_set_to_s }}
{% endif %}

{% if schema.column.collation %}
    COLLATE {{schema.column.collation_to_s }}
{% endif %}
;
