<?php

class ArrayObj
{
    static function array_push_recursive(&$array, $path, $object)
    {
        if (sizeof($path) <= 0) 
        {   
            if (!is_array($array)) $array = array();
            array_push($array, $object);
            return;
        }
        $key = array_shift($path);
        ArrayObj::array_push_recursive($array[$key], $path, $object);
    }

    static function init_keys_recursive(&$array, $keys)
    {
        if (sizeof($keys) <= 0) return;
        $key = array_shift($keys);
        if (!isset($array[$key])) $array[$key] = array();
        ArrayObj::init_keys_recursive($array[$key], $keys);
    }

    static function init_keys(&$array, $keys)
    {
        if (!is_array($keys)) $keys = array($keys);

        foreach ($keys as $key)
        {
            if (!isset($array[$key]))
            {
               $array[$key] = array(); 
            }
        }
    }

    static function diff($array1, $array2)
    {
        ArrayObj::init_keys($changes, array('keep', 'add', 'remove'));

        $mapping1 = array();
        ArrayObj::mapping($array1, $mapping1);
        $mapping2 = array();
        ArrayObj::mapping($array2, $mapping2);

        # determine what to keep, and add from array 1
        foreach($mapping1 as $path => $index)
        {
            if (isset($mapping2[$path]))
            {
                array_push($changes['keep'], $index);
                unset($mapping2[$path]);
            }
            else
            {
                array_push($changes['add'], $index);
                unset($mapping2[$path]);
            }
            
        }

       foreach($mapping2 as $path => $index)
       {
            array_push($changes['remove'], $index);
       }

        return $changes;
    }

    static function print_diff($changes)
    {
        $console = new Console();

        echo "New(". sizeof($changes['add']) .") " .
        "Removing(". sizeof($changes['remove']) .") " .
        "Reusing(". sizeof($changes['keep']) .")\n\n";

        foreach ($changes['add'] as $path => $value)
        {
            echo $console->color('green')->write_string('++ ') . 
                 $console->bold()->write_string($path) . "\n";
        }
    
        echo "\n";
    
        foreach ($changes['remove'] as $path => $value)
        {
            echo $console->color('red')->write_string('-- ') .
                 $path . "\n";
        }
    }

    static function mapping(
        &$array1, &$mapping, $path = array(), $path_index = array()
        )
    {

        if (!is_array($array1)) 
        {
            $array = array();
            ArrayObj::init_keys_recursive($array, $path);
            ArrayObj::array_push_recursive($array, $path, $array1);
            array_push($path, $array1);
            $mapping[join(' => ', $path)] = $array;

            return;
        }

        foreach ($array1 as $key => $index)
        {
            if (!is_integer($key)) array_push($path, $key);
            array_push($path_index, $key);
            ArrayObj::mapping($array1[$key], $mapping, $path, $path_index);
            array_pop($path_index);
            if (!is_integer($key)) array_pop($path);
        }
    }

}
