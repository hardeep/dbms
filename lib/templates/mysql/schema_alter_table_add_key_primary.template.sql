ALTER TABLE {{schema.object.safe_name}}
    ADD CONSTRAINT {{schema.key.alias}} PRIMARY KEY ({{schema.key.columns_to_s}})
;
