<?php
class SchemaSaveTest extends PHPUnit_Framework_TestCase
{
    function test_save_is_valid_php()
    {
        $this->person = new Person();
    }
}

class Person extends Dbms\Schema
{
    public function __construct()
    {
        parent::__construct();
        $this->add_attribute(
                "id",
                array (
                    'name' => 'id',
                    'type' => 'integer',
                    'not_null' => true,
                    'auto_inc' => true,
                    )
                );
        $this->add_attribute(
                "transaction_sha",
                array (
                    'name' => 'transaction_sha',
                    'type' => 'string',
                    'length' => '40',
                    )
                );
        $this->add_primary(
                array (
                    0 => 'id',
                    )
                );
        $this->add_unique(
                array (
                    0 => 'transaction_sha',
                    )
                );
        $this->add_foreign(
                $this->name,
                "table4",
                "table_id",
                "table_id"
                );
    }
}
