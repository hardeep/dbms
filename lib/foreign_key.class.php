<?php

namespace Dbms\Keys {

class ForeignKey extends Key
{
    function __construct()
    {
        parent::__construct();

        $this->attr_accessable(
            'referenced_columns',
            'referenced_table'
            );

        $this->before_filter(
            'validate_adapter',
            array('referenced_columns_to_s')
            );

        $this->before_set_filter(
            'safe_name',
            array('referenced_table', 'referenced_columns')
            );

        $this->before_set_filter(
            'validate_columns',
            'referenced_columns'
            );

        $this->after_get_filter(
            'set_referenced_columns',
            'referenced_columns'
            );

        $this->referenced_columns = null;
        $this->referenced_table = null;
    }

    protected function referenced_columns_to_s()
    {
        $class = $this->adapter_class;
        $q = $class::$qoute;
        return $q . implode("$q,$q", $this->referenced_columns) . $q;
    }

    public function alias_to_s($alias)
    {
        if (empty($alias))
        {
            return "fk_". $this->table . "_" .
            join('_', $this->columns) .
            "_on_" . $this->referenced_table . '_' .
            join('_', $this->referenced_columns);
        }

        return $alias;
    }

    protected function set_referenced_columns($args)
    {
        if (empty($args))
        {
            return $this->columns;
        }

        return $args;
    }
} # end ForeignKey

} # end Dbms
