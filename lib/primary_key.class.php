<?php

namespace Dbms\Keys {

class PrimaryKey extends Key
{
    function alias_to_s($alias)
    {
        if (empty($alias))
        {
            return "pk_". $this->table . "_" .join('_', $this->columns);
        }

        return $alias;
    }
}

}
