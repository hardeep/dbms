ALTER TABLE {{ schema.safe_name }}    
    CHANGE {{old_column.name_to_s}} {{new_column.name_to_s}} {{new_column.type_to_s}} {{new_column.length_to_s}}

    {{ new_column.not_null_to_s }}

    {% if new_column.auto_inc %}
    {{ new_column.auto_inc_to_s }}
    {% endif %}

    {% if new_column.default %}
    DEFAULT {{new_column.default_to_s}}
    {% endif %}

    {% if new_column.char_set %} 
    CHARACTER SET {{new_column.char_set_to_s}}
    {% endif %}

    {% if new_column.collation %}
    COLLATE {{new_column.collation_to_s }}
    {% endif %}
;
