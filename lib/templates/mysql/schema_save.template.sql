<?php

class {{schema.name}} extends Dbms\Schema
{
    public function __construct()
    {
        parent::__construct();

        {% foreach attribute in schema.attributes %}
        $this->add_attribute(
            "{{attribute.name}}",
            {{ attribute.properties.php_array() }}
            );
        {% endfor %}

        {% foreach key in schema.keys.primary %}
            $this->add_primary(
                {{ key.columns.php_array() }}
                );
        {% endfor %}

        {% foreach key in schema.keys.unique %}
            $this->add_unique(
                    {{key.columns.php_array()}}
                    );
        {% endfor %}

        {% foreach key in schema.keys.foreign %}
            $this->add_foreign(
                    $this->name,
                    "{{key.referenced_table}}",
                    {{key.columns.php_array()}},
                    {{key.referenced_columns.php_array()}}
                    );
        {% endfor %}
    }
}
