<?php

namespace Dbms {

use \InvalidArgumentException;
use \AutoLoader;

class Schema
{
    public $collation;
    public $char_set;
    public $db_adapter;
    public $adapter;
    public $attributes;
    public $keys;
    public $name;

    function __construct($name = null)
    {
        $this->name = (empty($name))?get_class($this):$name;

        $this->collation = 'utf8_unicode_ci';
        $this->char_set = 'utf8';
        $this->db_adapter = 'Mysql';

        # init the adapter to map types for each attribute
        $adapter_class  = __NAMESPACE__ . "\\Adapters\\" . $this->db_adapter;
        $adapter_class = AutoLoader::to_camel_case($adapter_class);
        $this->adapter = new $adapter_class();

        foreach ($this->adapter->key_types as $type => $properties)
        {
            $this->keys[$type] = null;
        }

        # by default we will include a primary key with a id
        $this->add_attribute(
            'id', 
            array(
                'type' =>'integer',
                'not_null' => true,
                'auto_inc' => true
                )
            );

        $this->add_primary('id');

        $this->add_attribute(
            'transaction_sha',
            array(
                'type' => 'string',
                'length' => '40'
                )
        );

       $this->add_unique('transaction_sha');

        return false;
    }

    function safe_name($schema_name = null)
    {
        if (empty($schema_name)) 
        {
            $schema_name = $this->name;
        }

        return AutoLoader::to_underscore_case(
            AutoLoader::class_name($schema_name)
            );
    }

    // Key Methods

    public function key_type_defined($type)
    {
        if (isset($this->adapter->key_types[$type])) return true;
        return false;
    }

    public function add_primary($columns, $options = null)
    {
        return $this->_add_key($columns, 'primary', false);
    }

    public function add_foreign(
            $table, $columns, $referenced_columns = null, $options = null
        )
    {
        $key = $this->_add_key($columns,'foreign');

        $this->keys['foreign'][$key->name]->referenced_columns = 
            $referenced_columns;

        # the table the foreign key references
        $this->keys['foreign'][$key->name]->referenced_table =
            $this->safe_name($table);

        return $key;
    }

    public function add_unique($columns, $options = null)
    {
        return $this->_add_key($columns,'unique');
    }

    private function _add_key(
          $attributes, $type, $allow_multiple = true, $options = array()
        )
    {
        if (!is_array($attributes)) $attributes = array($attributes);

        if (!$this->key_type_defined($type))
        {
            throw new InvalidArgumentException(
                "'$type' is not a valid key type"
                );
        }
        else
        {
            $key_class = "Dbms\\keys\\" . 
                AutoLoader::to_camel_case($type) . "Key";

            $key = new $key_class();

            # allow multiple is to disable multiple keys such as
            # having mulitple primary keys.

            if (!$allow_multiple)
            {
                if (isset($this->keys[$type]))
                {
                    unset($this->keys[$type]);
                }
            }

            (isset($options['key_prefix'])) or 
                ($options['key_prefix'] = null);

            $key->table = $this->safe_name($this->name);
            $key->name = join('_', $attributes);
            $key->columns = $attributes;
            $key->type = $type;
            $key->alias = $options['key_prefix'];
            $key->adapter = $this->adapter;

            \ArrayObj::init_keys($this->keys, $type);
            \ArrayObj::init_keys($this->keys[$type], $key->name);

            $this->keys[$type][$key->name] = $key; 

            return $key;
        }
    }

    // attributes

    function add_attribute($name, $options = null, $destroy = false)
    {
        $default_options = array(
            'type' => 'string',
            'length' => '100'
        );

        # if the user wants they can add multiple attributes
        # with the same options
        if (!is_array($name))
        {
            $name = array($name);
        }

        foreach ($name as $attr)
        {
            if ($options)
            {
                (isset($options['type'])) 
                    or ($options['type'] = $default_options['type']);

                $this->_handle_options($attr, $options, $destroy);
            }
            else
            {
                $this->_handle_options($attr, $default_options, $destroy);
            }
        }
    }

    function rename_attribute($old_name, $new_name)
    {
        $this->verify_has_attribute($old_name);

        $column = $this->attributes[$old_name];
        unset($this->attributes[$old_name]);
        # php tried to assign it by reference so I need to clone the
        # object

        $this->attributes[$new_name] = clone $column;
        $this->attributes[$new_name]->name = $new_name;

        return $column;
    }

    function remove_attribute($name)
    {
        $this->verify_has_attribute($name);

        unset($this->attributes[$name]);    
    }

    function remove_key($alias)
    {
        $key = clone $this->verify_has_key($alias);
        unset($this->keys[$key->type][$key->name]);
        return $key;
    }

    function verify_has_key($alias)
    {
        foreach($this->adapter->key_types as $type => $info)
        {
            if (isset($this->keys[$type]))
            {
                foreach ($this->keys[$type] as $key)
                {
                    if ($key->alias == $alias) return $key;
                }
            }
        }

        throw new InvalidArgumentException(
            "Key $alias not found."
            );
    }

    function verify_has_attribute($name)
    {
        if (!isset($this->attributes[$name])) 
        { 
            throw new InvalidArgumentException(
                    'Attribute does not exist: '. $name
                    );
        }
    }

    function _handle_options($attr, array $options, $destroy = false)
    {
        if ($destroy) 
        {
            unset($this->attributes[$attr]);
        }

        $column = new Column();
        $column->set_adapter($this->adapter);
        $column->name = $attr;

        foreach($options as $opt_type => $opts)
        {
            if (in_array($opt_type, $this->adapter->attr_properties))
            {
                # this is a know property set the attribute to include 
                # the properties
                # ex. $this->attributes['id']['type'] = 'integer';
                $column->$opt_type = $opts;
            }
            else
            {
                throw new InvalidArgumentException(
                        "'$opt_type' is not a supported property"
                    );
            }
        }

        $this->attributes[$attr] = $column;
    }

    function to_json()
    {
        $json = array();
        $json['keys'] = $this->keys;
        $json['properties'] = $this->attributes;
        return json_encode($json);
    }
} # end Schema

} # end Dbms
