<?php

class ForeignKeyTest extends PHPUnit_Framework_TestCase
{
    function setUp()
    {
       $this->key = new Dbms\Keys\ForeignKey(); 
       $this->key->adapter = new Dbms\Adapters\Mysql();
    }

    function test_safe_name()
    {
        $this->assertEquals(
            $this->key->safe_name("Hardeep's table version 2"),
            'hardeep_s_table_version_2'
            );

        $this->key->columns = array('ID', 'first name');

        $this->assertEquals(
            $this->key->columns,
            array('id', 'first_name')
        );
    }

    function test_adapter_class_has_been_set()
    {
        $this->assertEquals(
            $this->key->adapter_class,
            'Dbms\\Adapters\\Mysql'
            );
    }

    function test_columns_to_s()
    {
        $this->key->columns = array('id', 'address');
        $this->key->referenced_columns = array('id', 'address');
        $this->assertEquals(
            $this->key->columns_to_s(),
            '`id`,`address`'
            );
        $this->assertEquals(
            $this->key->referenced_columns_to_s(),
            '`id`,`address`'
            );
    }

    function test_no_referenced_keys_provided()
    {
        # expected to try and reference same columns from
        # referenced table
        $this->key->columns = 'id';
        $this->assertEquals(
            $this->key->referenced_columns,
            array('id')
            );
    }

    function test_key_returns_its_type()
    {
        $this->assertEquals(
            $this->key->type,
            'foreign'
            );
    }
}
