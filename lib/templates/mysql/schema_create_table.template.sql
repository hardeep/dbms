CREATE TABLE {{ schema.safe_name }}
(
  {% foreach attr in schema.attributes %}
    {{attr.name_to_s }} {{attr.type_to_s }} {{attr.length_to_s }}
        {{ attr.not_null_to_s }}
        {{ attr.default_to_s }}
        {{ attr.auto_inc_to_s }}
        {{ attr.char_set_to_s }}
        {{ attr.collation_to_s }}
        {% !if attr.last? %},{% endif %}
  {% endfor %}

  {% foreach key in schema.keys.primary %}
    , CONSTRAINT {{ key.alias }} PRIMARY KEY ({{ key.columns_to_s }})  
  {% endfor %}
  {% foreach key in schema.keys.unique %}
    , CONSTRAINT {{ key.alias }} UNIQUE KEY ({{ key.columns_to_s }})  
  {% endfor %}
  {% foreach key in schema.keys.foreign %}
    , CONSTRAINT {{ key.alias }} FOREIGN KEY ({{ key.columns_to_s }})
      REFERENCES {{ key.referenced_table }}({{key.referenced_columns_to_s}})
  {% endfor %}
)
CHARACTER SET {{ schema.char_set }} COLLATE {{ schema.collation }};
SET storage_engine={{ schema.adapter.engine }};

