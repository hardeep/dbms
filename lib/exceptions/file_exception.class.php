<?php
	class FileException extends Exception
	{
		public function __construct($e)
		{
			parent::__construct($e);
		}
		
		public function setMessage($e)
		{
			$this->mes = $e;
		}
		
		protected $mes;
	}
