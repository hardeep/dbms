<?php

namespace Dbms\Keys {

class UniqueKey extends Key
{
    function alias_to_s($alias)
    {
        if (empty($alias))
        {
            return "uk_". $this->table . "_" .join('_', $this->columns);
        }

        return $alias;
    }
}

}
