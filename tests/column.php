<?php

namespace Dbms {

use \PHPUnit_Framework_TestCase;

class ColumnTest extends PHPUnit_Framework_TestCase
{
    function test_object_exists()
    {
        $column = new Column(); 
        $this->assertEquals(get_class($column), 'Dbms\\Column');
    }

    function setUp()
    {
        $this->column = new Column(); 
    }

    function test_set_property()
    {
        $this->column->name = 'Name';
        $this->assertEquals($this->column->name, 'Name');
    }

    function test_parse_property()
    {
        $this->column->type = 'string';
        $this->column->length = 10;
        $this->column->default = "'cats'";
        $this->column->char_set = 'utf-8';
        $this->column->collation = 'utf8_bin';
        $this->column->not_null = true;
        $this->column->engine = 'innoDB';
        $this->column->auto_inc = true;

        $this->assertEquals($this->column->length, '10');
        $this->assertEquals($this->column->type, 'string');
        $this->assertEquals($this->column->default, "'cats'");
        $this->assertEquals($this->column->char_set, 'utf-8');
        $this->assertEquals($this->column->collation, 'utf8_bin');
        $this->assertEquals($this->column->not_null, true);
        $this->assertEquals($this->column->engine, 'innoDB');
        $this->assertEquals($this->column->auto_inc, true);
    }

    function test_to_s_functions()
    {
        $this->column->type = 'string';
        $this->column->length = 10;
        $this->column->default = "'cats'";
        $this->column->char_set = 'utf-8';
        $this->column->collation = 'utf8_bin';
        $this->column->not_null = true;
        $this->column->engine = 'innoDB';
        $this->column->auto_inc = true;

        $this->assertEquals($this->column->length_to_s(), '(10)');
        $this->assertEquals($this->column->type_to_s(), 'VARCHAR');
        $this->assertEquals($this->column->default_to_s(), "'cats'");
        $this->assertEquals($this->column->char_set_to_s(), 'utf-8');
        $this->assertEquals($this->column->collation_to_s(), 'utf8_bin');
        $this->assertEquals($this->column->not_null_to_s(), 'NOT NULL');
        $this->assertEquals($this->column->engine_to_s(), 'innoDB');
        $this->assertEquals(
            $this->column->auto_inc_to_s(), 'AUTO_INCREMENT'
            );
    }
} # end ColumnTest

} # end Dbms
