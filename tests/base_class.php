<?php

class Person extends Nodv\BaseClass
{
   public function __construct()
   {
       parent::__construct();
       $this->attr_accessable('id', 'first_name', 'last_name');
       $this->before_filter('capatalize', 'name_to_s');
       $this->id = 2;
   }

   protected function capatalize()
   {
       $this->first_name = strtoupper($this->first_name);
   }

   protected function safe_name($arg)
   {
       return strtolower($arg);
   }

   protected function name_to_s($arg = null)
   {
       (isset($arg)) or ($arg = $this->first_name);
       $q = Dbms\Adapters\Mysql::$qoute;
       return $q. $arg . $q;
   }

   private function bad_function()
   {
       return;
   }

   protected function say_cow($arg)
   {
       return "cow";
   }

   protected function say_moo($arg)
   {
       return $arg . ' says moo!';
   }
}

class BaseClassTest extends PHPUnit_Framework_TestCase
{
    function setUp()
    {
        $this->person = new Person();
        $this->assertEquals(
            get_class($this->person),
            'Person'
            );
    }

    function test_attribute_accessable()
    {
        $this->assertEquals(
            $this->person->id,
            2
            );
    }

    /**
    * @expectedException InvalidArgumentException
    * @expectedExceptionMessage Tried to access private or non 
    * existant element name
    */
    function test_not_accessable_property()
    {
        $this->person->name;
    }

    function test_before_filter()
    {
        $this->person->first_name = 'hardeep';

        $this->assertEquals(
            $this->person->first_name,
            'hardeep'
            );

        $this->assertEquals(
            $this->person->name_to_s(),
            '`HARDEEP`'
            );
    }

    /**
    * @expectedException InvalidArgumentException
    * @expectedExceptionMessage could cause a segfault 
    */
    function test_private_method_call()
    {
        $this->person->bad_function();
    }

    function test_no_namespace()
    {
        $this->assertEquals(
            $this->person->remove_namespace("Person::Method"),
            'Method'
            );

        $this->assertEquals(
            $this->person->remove_namespace("Recrod\Person::Method"),
            'Method'
            );
    }

    function test_after_get_filter()
    {
        $this->person->after_get_filter(
            array('say_cow', 'say_moo'),
            'id'
            );

        $this->assertEquals(
            $this->person->id,
            'cow says moo!'
            );
    }

    function test_before_set_filter()
    {
        $this->person->before_set_filter(
            array('safe_name', 'name_to_s'),
            'first_name'
            );
        $this->person->first_name = 'Hardeep';
        $this->assertEquals(
            $this->person->first_name,
            '`hardeep`'
            );
    }
}
