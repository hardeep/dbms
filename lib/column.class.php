<?php

namespace Dbms {

use Nodv\BaseClass;

class Column extends BaseClass
{
    public static $adapter = null;
    public $default_adapter;

    function __construct($name = null)
    {
        parent::__construct();

        $this->default_adapter = __NAMESPACE__ . "\\Adapters\\Mysql";
        $this->set_adapter(new $this->default_adapter());
        foreach(static::$adapter->attr_properties as $property)
        {
            $this->attr_accessable($property);
            $this->{$property} = null;
        }

        $this->attr_accessable('properties');

        $this->name = $name;
    }

    function set_adapter($adapter)
    {
        $this->destroy_filters("after_get_filter");

        static::$adapter = $adapter;

        foreach (static::$adapter->attr_properties as $property)
        {
            $this->attr_accessable($property);
        }
    }

    function name_to_s()
    {
        $args = $this->name;

        $class = get_class(self::$adapter);
        $q = $class::$qoute;

        if (empty($args)) return null;

        $safe_pattern = "/[^a-zA-Z0-9_]/";
        $replace_with = '_';

        return  $q .strtolower(
                preg_replace($safe_pattern, $replace_with, $args)
                ) . $q;
    }

    function type_to_s()
    {
        $type = $this->type;

        return static::$adapter->column_types[$type];
    }

    function length_to_s()
    {
        $length = $this->length;

        if (empty($length)) return null;
        return  "(". $length . ")";
    }

    function default_to_s()
    {
        $default = $this->default;

        if (empty($default)) return null; 

        if (strtoupper($default) == 'NULL' || $default == null)
        {
            return "NULL";
        } 
        else if (strtoupper($default) == 'NOT NULL') 
        {
            return "NOT NULL";
        }
        else
        {
            return $default;
        }
    }

    function not_null_to_s()
    {
        $null = $this->not_null;

        if (empty($null)) return null;

        if ($null == true)
        {
            return 'NOT NULL';
        }
        else if ($null == false)
        {
            return 'NULL';
        }
    }

    function auto_inc_to_s() 
    {
        $auto_inc = $this->auto_inc;

        if (empty($auto_inc)) return null;
        if ($auto_inc == true) return "AUTO_INCREMENT";
        return null;
    }

    function char_set_to_s()
    {
        $char_set = $this->char_set;

        return $char_set;
    }

    function collation_to_s()
    {
        $collation = $this->collation;

        return $collation;
    }

    function engine_to_s()
    {
        $engine = $this->engine;

        return $engine;
    }

    function properties()
    {
        $properties = array();

        foreach (static::$adapter->attr_properties as $property)
        {
            if (!is_null($this->{$property}))
            {
                $properties[$property] = $this->{$property};
            }
        }

        return $properties;
    }
} # end Column

} # end Dbms
